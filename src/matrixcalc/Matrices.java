/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrixcalc;

import java.util.Scanner;

/**
 *
 * @author si-mexon
 */
class Matrices {

    static int numberOfMatrices;

    public static int[][][] CreateMatrices() /*Creates matrices (with dimensions)*/{ 
        Scanner input = new Scanner(System.in);

        System.out.println("================MATRIX CALCULATOR================");
        System.out.println("Copyright: Simon Chukwu, 2017");
        System.out.println("=================================================");
        System.out.print("Number of Matrices: ");
        numberOfMatrices = input.nextInt();

        int[][][] Matrices = new int[numberOfMatrices + 1][][]; //added one (to the number of matrices) to store the matrix answer
        //Creates an array holding the Matrices depending on the number(of Matrices)
        
        for (int i = 0; i < (numberOfMatrices); i++) {
            //Sets the dimensions for the matrices
            int matChar = i + 1;
            System.out.print("\nDimension for Matrix" + matChar + ": ");
            int x = input.nextInt();
            int y = input.nextInt();

            Matrices[i] = new int[x][y];

            //to set values to the created matrices
            System.out.println("Enter the values of Matrix" + matChar + " in a row wise order.");
            for (int a = 0; a < x; a++) {
                for (int b = 0; b < y; b++) {
                    System.out.print("Matrix" + matChar + " [" + a + "][" + b + "]: ");
                    Matrices[i][a][b] = input.nextInt();
                }
            }

        }
        return Matrices;
    }
}
