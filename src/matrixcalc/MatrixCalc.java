/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrixcalc;

import java.util.Scanner;

/**
 *
 * @author si-mexon
 */
public class MatrixCalc {

    static int[][][] MatObject = Matrices.CreateMatrices();

    public static void main(String[] args) {

        String setOperation = DetOperation();
        DisplayMatAnswer(setOperation);

    }

    public static String DetOperation() {
        Scanner input = new Scanner(System.in);
        System.out.println("\n----------OPERATION----------");

        System.out.println("For Addition enter 1");
         System.out.println("For Subtraction enter 2");
        if (Matrices.numberOfMatrices <= 2) {
            System.out.println("For Multiplication enter 3");
            System.out.println("For Division enter 4");
        }
        System.out.print(":? ");

        switch (input.nextInt()) {
            case 1:
                return "Addition";
            case 2:
                return "Subtraction";
            case 3:
                return "Multiplication";
            case 4:
                return "Division";
            default:
                break;
        }
        return null;
    }

    public static void DisplayMatAnswer(String a) {
        System.out.print("\n----------MATRIX ANSWER----------");
        int[][] answer = null;

        if ("Addition".equals(a)){
            answer = Operations.MatrixAddition();
        } 
        if ("Subtraction".equals(a)){
            answer = Operations.MatrixSubtraction();
        }
        if ("Multiplication".equals(a)){
            answer = Operations.MatrixMultiplication();
        }
        if ("Division".equals(a)){
            answer = Operations.MatrixDivision();
        }
        

        System.out.println("");
        int x = MatObject[0].length; //row of the first matrix is stored as x
        int y = MatObject[0][0].length; // column of the first matrix is stored as y

        for (int m = 0; m < x; m++) {
            for (int n = 0; n < y; n++) {
                System.out.print(answer[m][n]);
                System.out.print("\t");
            }
            System.out.println();
        }
    }

}
