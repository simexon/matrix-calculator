/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrixcalc;

/**
 *
 *
 * @author si-mexon
 */
public class Operations extends MatrixCalc {

    public static boolean isValidAddition(int numberOfMatrices) {
//        int[][][] Mat = Matrices.CreateMatrices();
        for (int i = 0; i < numberOfMatrices; i++) {
            System.out.println(MatrixCalc.MatObject[i].length + " by " + MatObject[i][0].length);
            int x = MatObject[i].length; //row of the first matrix is stored as x
            int y = MatObject[i][0].length; // column of the first matrix is stored as y
            int a = ++i; //increases i by 1 before using it 
            System.out.println(MatObject[a].length + " by " + MatObject[a][0].length);

            if ((x == MatObject[a].length) && (y == MatObject[a][0].length)) {
                System.out.println("Both matrices are the same");
                return true;
            } else {
                System.out.println("Matrices are not the same");
                return false;
            }
        }
        return false;
    }

    public static boolean IsValidMultiplication() {
        return false;
    }

    public static int[][] MatrixAddition() {
        int x = MatObject[0].length; //row of the first matrix is stored as x
        int y = MatObject[0][0].length; // column of the first matrix is stored as y

        int[][] MatAnswer = new int[x][y];
        for (int m = 0; m < x; m++) {
            for (int n = 0; n < y; n++) {
                int sum = 0;
                for (int i = 0; i < Matrices.numberOfMatrices; i++) {
                    sum += MatObject[i][m][n];
                    MatAnswer[m][n] = sum;

                }

            }

        }

        return MatAnswer;
    }

    public static int[][] MatrixSubtraction() {
        int x = MatObject[0].length; //row of the first matrix is stored as x
        int y = MatObject[0][0].length; // column of the first matrix is stored as y

        int[][] MatAnswer = new int[x][y];
        for (int m = 0; m < x; m++) {
            for (int n = 0; n < y; n++) {
                int sub = MatObject[0][m][n];
                for (int i = 1; i < Matrices.numberOfMatrices; i++) {
                    sub -= (MatObject[i][m][n]);
                    MatAnswer[m][n] = sub;
                }

            }

        }

        return MatAnswer;
    }

    public static int[][] MatrixMultiplication() {
        //before multipliying the matrices, we have to check for the validity of the Matricse to be multiplied

        return null;
    }

    public static int[][] MatrixDivision() {
        return null;
    }
}
